﻿namespace Bioscan.Backend.Api.Middleware
{
    public class MyAuthenticationMiddleware //TODO ФИКСИТЬ ВСЕ ВАРНИНГИ И СООБЩЕНИЯ ПО ВСЕКМУ КОДУ
    {
        private readonly RequestDelegate _next;

        public MyAuthenticationMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            var user = context.User;
            {
                var isAuthenticated = user.Identity.IsAuthenticated;
                var userClaims = user.Claims;
            }
            await _next(context);
        }
    }

    public static class Extensions
    {
        public static IApplicationBuilder UseMyAuthenticationMiddleware(this IApplicationBuilder builder) => builder == null
                ? throw new ArgumentNullException(nameof(builder))
                : builder.UseMiddleware<MyAuthenticationMiddleware>();

    }
}
