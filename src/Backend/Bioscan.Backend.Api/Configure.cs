﻿using Bioscan.Backend.Api.JwtBearer;
using Bioscan.Backend.DataAccess.Ef;
using Bioscan.Backend.DataAccess.Repositories;
using Bioscan.Backend.Domain.Entities;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Net.Http.Headers;
using System.Text;

namespace Bioscan.Backend.Api
{
    public static class Configure
    {
        public static void Postgres(WebApplicationBuilder builder)
        {
            DbContextOptionsBuilder<BioscanDbContext> optionsBuilder = new();
            DbContextOptions<BioscanDbContext> options = optionsBuilder.UseNpgsql(builder.Configuration.GetConnectionString("Local")).Options;
            builder.Services.AddSingleton(typeof(DbContext), new BioscanDbContext(options));
            builder.Services.AddSingleton<IRepository<User>, BioscanEfRepository<User>>();
            builder.Services.AddSingleton<IRepository<SurveyProgram>, BioscanEfRepository<SurveyProgram>>();
            builder.Services.AddSingleton<IRepository<Analysis>, BioscanEfRepository<Analysis>>();

        }
        public static void Jwt(WebApplicationBuilder builder)
        {
            builder.Services.Configure<JwtOptions>(builder.Configuration.GetSection(nameof(JwtOptions)));
            builder.Services.AddScoped<IJwtProvider, JwtProvider>();
        }

        public static void ApiAuthentication(WebApplicationBuilder builder)
        {
            //Можно быстрее добираться "Jwt:Key"
            var sKey = builder.Configuration.GetSection(nameof(JwtOptions)).GetSection(nameof(JwtOptions.SecretKey));

            //AddJwtBearer обертка над AddScheme
            builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(JwtBearerDefaults.AuthenticationScheme,
                options =>
                {
                    if (sKey.Value != null)
                    {
                        options.TokenValidationParameters = new TokenValidationParameters()
                        {
                            ValidateIssuer = false,
                            ValidateAudience = false,
                            ValidateLifetime = true,
                            ValidateIssuerSigningKey = true,
                            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(sKey.Value)),
                        };

                        //Пример как сделать через стоковый Middleware без кастомного

                        //options.Events = new JwtBearerEvents()
                        //{
                        //    OnMessageReceived = context =>
                        //    {
                        //        context.Token = context.Request.Cookies["ocean"];
                        //        return Task.CompletedTask;
                        //    }
                        //};

                    }
                });

            builder.Services.AddAuthorization();

        }
    }
}
