﻿namespace Bioscan.Backend.Api.Helpers
{
    public static class PasswordHelper
    {
        public static string HashPassword(string password) => BCrypt.Net.BCrypt.EnhancedHashPassword(password);

        public static bool Verify(string password, string hashedPass) => BCrypt.Net.BCrypt.EnhancedVerify(password, hashedPass);

    }
}
