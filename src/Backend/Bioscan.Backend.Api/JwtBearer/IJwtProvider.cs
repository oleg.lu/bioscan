﻿using Bioscan.Backend.Domain.Entities;

namespace Bioscan.Backend.Api.JwtBearer
{
    public interface IJwtProvider
    {
        public string GenerateToken(User user);
    }
}
