﻿using Bioscan.Backend.Domain.Entities;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Bioscan.Backend.Api.JwtBearer
{
    public class JwtProvider : IJwtProvider
    {
        private readonly JwtOptions _options;

        public JwtProvider(IOptions<JwtOptions> options) => _options = options.Value;

        public string GenerateToken(User user)
        {
            Claim[] claims =
            {
                new("userId", user.Id.ToString()),
                user.Login.StartsWith("a_") ?
                    new Claim(ClaimTypes.Role,"Admin")
                    :
                    new Claim(ClaimTypes.Role,"User")
            };

            var signingCredentials = new SigningCredentials(
                new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_options.SecretKey))
                , SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                claims: claims, //Claim может содержать в себе ключ значение, туда передаются какие либо параметры для АУНТЕФИКАЦИИ
                signingCredentials: signingCredentials,
                expires: DateTime.UtcNow.AddHours(_options.ExpiresHours));

            var tokenValue = new JwtSecurityTokenHandler().WriteToken(token);

            return tokenValue;
        }
    }
}
