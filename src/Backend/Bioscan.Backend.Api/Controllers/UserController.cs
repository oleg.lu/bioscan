﻿using Bioscan.Backend.Api.Helpers;
using Bioscan.Backend.Api.JwtBearer;
using Bioscan.Backend.DataAccess.Repositories;
using Bioscan.Backend.Domain.Entities;
using Microsoft.AspNetCore.Mvc;

namespace Bioscan.Backend.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IRepository<User> _userRepository;
        private readonly IJwtProvider _jwtProvider;

        public UserController(IRepository<User> userRepo, IJwtProvider jwtProvider)
        {
            _userRepository = userRepo;
            _jwtProvider = jwtProvider;
        }

        [HttpPost]
        [Route("~/Register")]
        public async Task<IActionResult> RegisterNewUserAsync(string email, string login, string password)
        {
            User user = new()
            {
                Email = email,
                Login = login,
                HashPassword = PasswordHelper.HashPassword(password),
            };

            var id = await _userRepository.AddNewEntityAndGetIdAsync(user);

            return Ok(id);
        }

        [HttpGet]
        //[Route("Login")]
        [Route("~/Login")] //- с игнорированием базового маршрута
        public async Task<IActionResult> Login(string login, string password)
        {
            User? userByLogin = await _userRepository.GetEntityByStringFieldAsync(nameof(Domain.Entities.User.Login), login);

            if (userByLogin == null) return NotFound("Неверный Логин");

            var res = PasswordHelper.Verify(password, userByLogin.HashPassword);

            if (!res) return Unauthorized("Неверный пароль");

            var token = _jwtProvider.GenerateToken(userByLogin);

            return Ok(token);
        }


    }

}
