﻿using Bioscan.Backend.DataAccess.Repositories;
using Bioscan.Backend.Domain.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Bioscan.Backend.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SurveyProgramController : ControllerBase
    {
        private readonly IRepository<SurveyProgram> _suProgramRepo;

        public SurveyProgramController(IRepository<SurveyProgram> suProgramRepo)
        {
            _suProgramRepo = suProgramRepo;
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> AllSurveyPrograms()
        {
            var suPrograms = await _suProgramRepo.GetAllAsync();

            return Ok(suPrograms);
        }

        [Route("GetAdmin")]
        [Authorize(Roles = "Admin")]
        [HttpGet]
        public async Task<IActionResult> AllSurveyProgramsAdmin()
        {
            var suPrograms = await _suProgramRepo.GetAllAsync();
            foreach (var su in suPrograms)
            {
                //su.GroupTest = "admin";
            }

            return Ok(suPrograms);
        }

    }
}
