﻿using Bioscan.Backend.DataAccess.Repositories;
using Bioscan.Backend.Domain.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Bioscan.Backend.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AnalysisController : ControllerBase
    {
        private readonly IRepository<Analysis> _analysisRepo;

        public AnalysisController(IRepository<Analysis> analysisRepo)
        {
            _analysisRepo = analysisRepo;
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> AllSurveyAnalyzes()
        {
            var analyzes = await _analysisRepo.GetAllAsync();

            return Ok(analyzes);
        }
    }
}
