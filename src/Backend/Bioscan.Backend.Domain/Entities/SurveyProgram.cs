﻿namespace Bioscan.Backend.Domain.Entities
{
    public class SurveyProgram : BaseEntity
    {
        public required string Name { get; set; }
        public string? Description { get; set; }
        public int? ProfessionalAppointmentsCnt { get; set; }
        public int? AnalysisCnt { get; set; }
        public double? Price { get; set; }

    }
}
