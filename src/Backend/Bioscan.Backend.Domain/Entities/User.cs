﻿namespace Bioscan.Backend.Domain.Entities
{
    public class User : BaseEntity
    {
        public required string Login { get; set; }
        public required string Email { get; set; }
        public required string HashPassword { get; set; }

    }
}
