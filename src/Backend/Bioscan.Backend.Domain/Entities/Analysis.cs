﻿namespace Bioscan.Backend.Domain.Entities
{
    public class Analysis : BaseEntity
    {
        public required string Name { get; set; }
        public string? Nomenclature { get; set; }
        public string? Description { get; set; }
        public double? Price { get; set; }
        public int ProductionDay { get; set; }

    }
}
