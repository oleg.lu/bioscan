﻿namespace Bioscan.Backend.Domain.Entities
{
    public class BaseEntity
    {
        public int Id { get; set; }
    }
}
