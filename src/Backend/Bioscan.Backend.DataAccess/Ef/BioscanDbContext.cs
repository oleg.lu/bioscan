﻿using Bioscan.Backend.DataAccess.DbData;
using Bioscan.Backend.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Bioscan.Backend.DataAccess.Ef
{
    public sealed class BioscanDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<SurveyProgram> SurveyPrograms { get; set; }
        public DbSet<Analysis> Analyzes { get; set; }

        public BioscanDbContext(DbContextOptions<BioscanDbContext> options) : base(options)
        {
            Database.EnsureDeleted();
            Database.EnsureCreated();
            Seed();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //modelBuilder.Entity<User>(entity =>
            //{
            //    entity.Property(e => e.Login)
            //        .IsRequired();

            //    entity.Property(e => e.Email)
            //        .IsRequired();

            //    entity.Property(e => e.HashPassword)
            //        .IsRequired();
            //});

        }

        private void Seed()
        {
            Users.AddRange(FakeDataFactory.Users);
            SurveyPrograms.AddRange(FakeDataFactory.SurveyPrograms);
            Analyzes.AddRange(FakeDataFactory.Analyzes);
            SaveChanges();
        }
    }
}
