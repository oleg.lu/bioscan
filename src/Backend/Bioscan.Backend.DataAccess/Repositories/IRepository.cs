﻿using Bioscan.Backend.Domain.Entities;

namespace Bioscan.Backend.DataAccess.Repositories
{
    public interface IRepository<T> where T : BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();
        Task<T> GetByIdAsync(int id);
        public Task<T?> GetEntityByStringFieldAsync(string propertyName, string propertyValue);
        public Task<int> AddNewEntityAndGetIdAsync(T entity); //OLD //public async Task<int> AddNewEntityAndGetIdAsync<TE>(TE entity) where TE : BaseEntity
        Task<bool> DeleteEntityByIdAsync(int id);

    }
}
