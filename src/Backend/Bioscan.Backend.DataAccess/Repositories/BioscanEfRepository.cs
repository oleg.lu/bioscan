﻿using Bioscan.Backend.DataAccess.Ef;
using Bioscan.Backend.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;
using System.Reflection;

namespace Bioscan.Backend.DataAccess.Repositories
{
    public class BioscanEfRepository<T> : IRepository<T> where T : BaseEntity
    {

        private readonly BioscanDbContext _dbContext;

        public BioscanEfRepository(DbContext dbContext)
        {
            _dbContext = (BioscanDbContext)dbContext;
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var ctx = _dbContext.Set<T>();

            var res = await ctx.Select(x => x).ToListAsync();

            return res;
        }

        public Task<T> GetByIdAsync(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<T?> GetEntityByStringFieldAsync(string propertyName, string propertyValue)
        {
            var ctx = _dbContext.Set<T>();

            Type type = typeof(T);
            PropertyInfo? property = type.GetProperty(propertyName);

            if (property == null) throw new Exception("E1. Unknown propertyName");

            var parameter = Expression.Parameter(type, "x");
            var prop = Expression.Property(parameter, propertyName);
            var constant = Expression.Constant(propertyValue);
            var equal = Expression.Equal(prop, constant);
            var lambda = Expression.Lambda<Func<T, bool>>(equal, parameter);
            var res = await ctx.FirstOrDefaultAsync(lambda);

            return res;
        }

        public async Task<int> AddNewEntityAndGetIdAsync(T entity)
        {
            //DbSet<TE>? focusDbSet = typeof(TE) switch
            //{
            //    Type t when t == typeof(User) => _dbContext.Users as DbSet<TE>,
            //    _ => throw new InvalidOperationException("Unknown entity type")
            //};

            //if (focusDbSet != null)
            //{
            //    TE addedEntity = focusDbSet.Add(entity).Entity;
            //    await _dbContext.SaveChangesAsync();
            //    return addedEntity.Id;
            //}

            //throw new InvalidOperationException("Unknown entity type");

            var ctx = _dbContext.Set<T>();
            T addedEntity = ctx.Add(entity).Entity;
            await _dbContext.SaveChangesAsync();
            return addedEntity.Id;

        }

        public Task<bool> DeleteEntityByIdAsync(int id)
        {
            throw new NotImplementedException();
        }
    }
}
