﻿using Bioscan.Backend.Domain.Entities;

namespace Bioscan.Backend.DataAccess.DbData
{
    public class FakeDataFactory
    {
        public static IEnumerable<User> Users => new List<User>()
        {
          new() {  Login = "test",HashPassword = "$2a$11$nKeEyRnj3cstAi54FoBo9Oo9oku.H/14BejBciEoqxKk4ZP0pIS.K",Email = "t@aa"}, //pass test
          new() {  Login = "test2",HashPassword = "$2a$11$nKeEyRnj3cstAi54FoBo9Oo9oku.H/14BejBciEoqxKk4ZP0pIS.K",Email = "t2@aa"},//pass test
          new() {  Login = "test3",HashPassword = "$2a$11$nKeEyRnj3cstAi54FoBo9Oo9oku.H/14BejBciEoqxKk4ZP0pIS.K",Email = "t3@aa"} ,//pass test
          new() {  Login = "a_test",HashPassword = "$2a$11$nKeEyRnj3cstAi54FoBo9Oo9oku.H/14BejBciEoqxKk4ZP0pIS.K",Email = "admin@aa"} ,//pass test
        };

        public static IEnumerable<SurveyProgram> SurveyPrograms => new List<SurveyProgram>
        {
            new()
            {
                Name = "Комплексная программа обследования мужчин старше 25 лет",
                Description = "Программа позволяет выявить возможные заболевания и риски, характерные для возраста 25+, при помощи лабораторных и функциональных исследований, а также консультаций ведущих специалистов.",
                ProfessionalAppointmentsCnt = 5,
                AnalysisCnt = 10,
                Price = 35_560,
            },
            new()
            {
                Name = "Комплексная программа обследования мужчин старше 40 лет",
                Description = "Программа направлена на оценку липидного обмена, выявление угрозы формирования сахарного диабета 2-го типа, диагностику возможных нарушений мозгового кровообращения, углубленное андрологическое обследование и эндоскопический контроль толстого кишечника.",
                ProfessionalAppointmentsCnt = 6,
                AnalysisCnt = 14,
                Price = 52_560,
            },
            new()
            {
                Name = "Программа обследования сердечно-сосудистой системы",
                Description = "Программа фокусируется на выявлении сердечно-сосудистых заболеваний, включая обследование сердца, артерий и общего состояния кровообращения.",
                ProfessionalAppointmentsCnt = 4,
                AnalysisCnt = 8,
                Price = 28_900,
            },
            new()
            {
                Name = "Программа диагностики заболеваний печени",
                Description = "Комплексное обследование для раннего выявления патологий печени и желчевыводящих путей с использованием УЗИ и лабораторных анализов.",
                ProfessionalAppointmentsCnt = 3,
                AnalysisCnt = 7,
                Price = 24_300,
            },
            new()
            {
                Name = "Программа раннего выявления онкологических заболеваний",
                Description = "Специальная программа для выявления онкологических заболеваний на ранней стадии, включая анализы и визуализационные исследования.",
                ProfessionalAppointmentsCnt = 6,
                AnalysisCnt = 12,
                Price = 45_700,
            },
            new()
            {
                Name = "Программа диагностики заболеваний щитовидной железы",
                Description = "Комплексная диагностика состояния щитовидной железы с использованием ультразвука и анализа гормонов.",
                ProfessionalAppointmentsCnt = 2,
                AnalysisCnt = 6,
                Price = 19_900,
            },
            new()
            {
                Name = "Программа обследования для мужчин старше 50 лет",
                Description = "Комплексная программа для мужчин старше 50 лет с целью диагностики возрастных изменений, включая сердечно-сосудистые и эндокринные нарушения.",
                ProfessionalAppointmentsCnt = 7,
                AnalysisCnt = 15,
                Price = 59_800,
            },
            new()
            {
                Name = "Программа диагностики заболеваний опорно-двигательного аппарата",
                Description = "Программа для выявления заболеваний костной системы, включая артриты, остеопороз и дегенеративные изменения суставов.",
                ProfessionalAppointmentsCnt = 4,
                AnalysisCnt = 8,
                Price = 31_400,
            },
            new()
            {
                Name = "Программа обследования нервной системы",
                Description = "Комплексное обследование состояния нервной системы с использованием электроэнцефалографии и консультаций невролога.",
                ProfessionalAppointmentsCnt = 3,
                AnalysisCnt = 9,
                Price = 38_200,
            },
            new()
            {
                Name = "Программа диагностики заболеваний ЖКТ",
                Description = "Программа для диагностики заболеваний желудочно-кишечного тракта, включая гастроскопию и лабораторные исследования.",
                ProfessionalAppointmentsCnt = 5,
                AnalysisCnt = 11,
                Price = 47_600,
            }

        };

        public static IEnumerable<Analysis> Analyzes => new List<Analysis>
        {
            new()
            {
                Name = "АКТГ (Адренокортикотропный гормон, кортикотропин, Adrenocorticotropic Hormone, ACTH)",
                Nomenclature = "A09.05.067 (Номенклатура МЗ РФ, Приказ №804н)",
                Description = "Для выявления недостаточности коры надпочечников, для диагностики болезни и различных форм синдрома Иценко-Кушинга.",
                ProductionDay = 1,
                Price = 1_160,
            },
            new()
            {
                Name = "Кортизол (Гидрокортизон, Cortisol)",
                Nomenclature = "A09.05.135 (Номенклатура МЗ РФ, Приказ №804н)",
                Description = "Для диагностики и динамического наблюдения терапии болезни и синдрома Иценко-Кушинга, болезни Аддисона.",
                ProductionDay = 1,
                Price = 745,
            },
            new()
            {
                Name = "Свободный кортизол (суточная моча) (Free cortisol, urine)",
                Nomenclature = null,
                Description = "Для диагностики болезни и синдрома Иценко-Кушинга, для дифференциальной диагностики  синдрома Кушинга и простого ожирения.",
                ProductionDay = 2,
                Price = 1_160,
            },

        };

    }
}
