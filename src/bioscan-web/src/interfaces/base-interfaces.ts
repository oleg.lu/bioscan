interface CheckUp {
    name: string;
    description: string;
    professionalAppointmentsCnt: number;
    analysisCnt: number;
    price: number;
    id: number;
}

interface Analysis {
    name: string;
    nomenclature: string;
    description: string;
    productionDay: number;
    price: number;
    id: number;
}