import { Button } from 'antd';
import { ReactNode } from 'react';
import '../style/component-style.scss';
import AuthInterceptor from '../gears/auth-interceptor';

export function HomeTestForm(): ReactNode {

    const handleGetSuProgramsClick = async () => {
        AuthInterceptor.get('/SurveyProgram/GetPublic')
            .then(response => {
                console.log(response.data);
            })
            .catch(error => {
                console.error('Error:', error);
            });
    };
//todo 123
    const handleGetSuAdminProgramsClick = async () => {
        AuthInterceptor.get('/SurveyProgram/GetAdmin')
            .then(response => {
                console.log(response.data);
            })
            .catch(error => {
                console.error('Error:', error);
            });
    };

    return (
        <div >
            <h1>HOME TEST PAGE</h1> 
            <Button onClick={handleGetSuProgramsClick}>GetSuprogramsPublic</Button>
            <Button onClick={handleGetSuAdminProgramsClick}>GetSuprogramsAdmin</Button>

        </div>
    )
}
