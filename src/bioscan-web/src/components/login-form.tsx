import { Input, Button } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import  { ReactNode, useState } from 'react';
import '../style/component-style.scss';
import { useNavigate } from 'react-router-dom';

const BASE_URL = "https://localhost:7189";


export function LoginForm(): ReactNode {

    const [login, setLogin] = useState<string>('');
    const [password, setPassword] = useState<string>('');
    const [error, setError] = useState<string>('');
    const navigate = useNavigate();


    const handleLoginClick = async () => {
        try {
            const response = await fetch(`${BASE_URL}/Login?login=${(login)}&password=${(password)}`, { 
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                },
                credentials: 'include', // Чтобы куки передавались с запросом
            });
            
            if (response.ok) {
                var token = await response.text();
                localStorage.setItem('ocean2', token);
                setError('');
                navigate('/');
            } else {
                // Обработка ошибок
                const errorMessage = await response.text();
                setError(errorMessage);
            }
        } catch (error) {
            console.error('Ошибка аутентификации:', error);
            setError('Ошибка. Попробуйте еще раз.');
        }
    };

    return (
        <div className='login-form'>
            <div className='login-title'>Вход в Bioscan</div>

            <Input className='login-input' size="large" placeholder="Логин" prefix={<UserOutlined />}
                value={login} onChange={(e) => setLogin(e.target.value)} required />

            <Input.Password className='login-input' placeholder="Пароль"
                value={password} onChange={(e) => setPassword(e.target.value)} required />

            <Button className='login-button' type="primary"
                onClick={handleLoginClick}>Вход</Button>

            {error && <p style={{ color: 'red' }}>{error}</p>}
        </div>
    )
}
