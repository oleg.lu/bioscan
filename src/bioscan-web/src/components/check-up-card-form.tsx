import { ReactNode } from 'react';
import '../style/component-style.scss';
import { bcolors } from '../style/bcolors.ts';
import { Button, Card, Col, Row, Typography } from 'antd';

const { Text, Title } = Typography;


export function CheckUpCardForm({
    name,
    description,
    professionalAppointmentsCnt,
    analysisCnt,
    price,
}: CheckUp): ReactNode {
    return (
        <Card>
            <Row>
                <Col xs={{ span: 15, offset: 1 }}>
                    <Row >
                        <Title style={{ color: bcolors.primaryInvitro }} level={2}>{name}</Title>
                    </Row>

                    <Row>
                        <Text>
                            {description}
                        </Text>
                    </Row>

                    <Row style={{ marginTop: '20px' }}>
                        <Text>Комплексные обследования выполняются по адресу:</Text>
                        <a href="https://maps.google.com/?q=Москва, Каширское ш., д. 22 корп. 2">
                            г. Москва, Каширское ш., д. 22 корп. 2
                        </a>
                    </Row>

                    <Row align="middle">
                        <Col >
                            <Title level={2} style={{ color: bcolors.primaryInvitro }}>{professionalAppointmentsCnt}</Title>
                        </Col>
                        <Col style={{ marginLeft: '10px' }}>
                            <Text type="secondary">приемов у специалистов</Text>
                        </Col>

                        <Col style={{ marginLeft: '40px' }}  >
                            <Title level={2} style={{ color: bcolors.primaryInvitro }}>{analysisCnt}</Title>
                        </Col>
                        <Col style={{ marginLeft: '10px' }}>
                            <Text type="secondary">методов диагностики</Text>
                        </Col>
                    </Row>
                </Col>

                <Col xs={{ offset: 2 }}>
                    <Row justify="space-between" align="middle" style={{ marginTop: '20%' }}>
                        <Title style={{ color: bcolors.primaryInvitro }} level={3}>{price} руб.</Title>
                    </Row>
                    <Button type="primary" ghost block shape="round" style={{ marginTop: '20%' }}>
                        Записаться
                    </Button>
                </Col>
            </Row>
        </Card>
    );
}
