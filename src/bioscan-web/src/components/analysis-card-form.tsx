import { ReactNode } from 'react';
import '../style/component-style.scss';
import { Card, Button, Row, Col, Typography, Divider } from 'antd';
import { ClockCircleOutlined, PlusOutlined,FieldNumberOutlined } from '@ant-design/icons';


const { Text, Title } = Typography;


export function AnalysisCardForm({
    name,
    nomenclature,
    description,
    productionDay,
    price,
    id,
}: Analysis): ReactNode {
// export function AnalysisCardForm(): ReactNode {

    return (
        <Card style={{ width: 980 }}>
            <Row>
                <Col span={18}>
                    <Title level={4}>{name}</Title>
                    <Title level={5}>{nomenclature}</Title>
                    <Text>
                    {description}
                    </Text>
                </Col>

                <Col>
                    <Divider type="vertical" style={{ height: '100%' }} />
                </Col>

                <Col style={{ paddingLeft: '20px' }} >

                    <Row justify="end">
                        <Col>
                            <Text>#{id}</Text>
                        </Col>
                        <Col>
                            <FieldNumberOutlined style={{ paddingLeft: '20px' }}/>
                        </Col>
                    </Row>

                    <Row justify="end">
                        <Col>
                            <Text>Дней на изготовление: {productionDay}</Text>
                            <ClockCircleOutlined style={{ paddingLeft: '20px' }}/>
                        </Col>
                    </Row>

                    <Row style={{ position: 'absolute', bottom: 0, width: '100%' }} justify="space-between" align="middle">
                            <Title level={2}>{price} ₽</Title>
                            <Button size="large" type="primary"  icon={<PlusOutlined />} />  
                    </Row>

                </Col>
            </Row>
        </Card>
    );
}
