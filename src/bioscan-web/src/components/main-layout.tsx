import { ReactNode } from 'react';
import { Layout, Input, Button, Flex } from 'antd';
import { Link } from 'react-router-dom';
import { RollbackOutlined } from '@ant-design/icons';
import '../style/component-style.scss';


const { Header, Content, Footer } = Layout;
const { Search } = Input;

interface MainLayoutProps {
    children: ReactNode;
}

export function MainLayout({ children }: MainLayoutProps): ReactNode {

    const handleLogoutClick = async () => {
        localStorage.removeItem('ocean2');
    };

    return (
        <Layout style={{ minHeight: '100vh' }}>
            <Header style={{ height: '5%', backgroundColor: 'white', }}>
                <Flex style={{ marginTop: '1%', marginLeft: 'auto', marginRight: 'auto', width: '35%' }}>
                    <Search size="large" placeholder="Поиск на сайте" onSearch={value => console.log(value)} />
                </Flex>

                <Flex justify='center' align='flex-start'>
                    <Link to="/">
                        <Button type="link" >На главную</Button>
                    </Link>
                    <Link to="/analysis">
                        <Button type="link">Анализы</Button>
                    </Link>
                    <Link to="/checkUps">
                        <Button type="link">Программы здоровья</Button>
                    </Link>
                    <Link to="/login">
                        <Button onClick={handleLogoutClick} type="primary" shape="round" icon={<RollbackOutlined />}>
                            Logout
                        </Button>
                    </Link>
                </Flex>
            </Header>

            <Content >
                {children}
            </Content>

            <Footer style={{ textAlign: 'center' }}>
                FOOTER ©{new Date().getFullYear()}
            </Footer>
        </Layout>
    );
};