import axios from 'axios';

const AuthInterceptor = axios.create({
    baseURL: "https://localhost:7189",
});

AuthInterceptor.interceptors.request.use(
    (config) => {
        const token = localStorage.getItem('ocean2');
        if (token != null) {
            config.headers.Authorization = `Bearer ${token}`;
        }
        return config;
    },
    (error) => {
        return Promise.reject(error);
    }
);

export default AuthInterceptor;