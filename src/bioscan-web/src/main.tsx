import React from 'react'
import ReactDOM from 'react-dom/client'
import { RouterProvider, createBrowserRouter } from 'react-router-dom';
import ErrorPage from './pages/Error';
import { LoginPage } from './pages/Login';
import HomeTestPage from './pages/HomeTest';
import HomePage from './pages/Home';
import CheckUpsPage from './pages/CheckUps';
import AnalysisPage from './pages/Analyzes';

const router = createBrowserRouter([
  {
    path: "/",
    element: <HomePage />,
    errorElement: <ErrorPage />,
  },
  {
    path: "/test",
    element: <HomeTestPage />,
  },
  {
    path: "/login",
    element: <LoginPage />,
  },
  {
    path: "/checkUps",
    element: <CheckUpsPage />,
  },
  {
    path: "/analysis",
    element: <AnalysisPage />,
  },
]);

ReactDOM.createRoot(document.getElementById('root')!).render(
  //321
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>,
)

//Выше современный формат, ниже старый

// import React from "react";
// import ReactDOM from "react-dom/client";
// import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";
// import { LoginPage } from './pages/Login/index';
// import HomePage from "./pages/Home";
// import App from "./App";
// import ErrorPage from "./pages/Error";


// const root = ReactDOM.createRoot(document.getElementById("root")!);
// root.render(
//   <React.StrictMode>
//     <BrowserRouter>
//       <Routes>
//         <Route index element={<HomePage />} />
//         <Route path="login" element={<LoginPage />} />
//         <Route path="a" element={<App/>} />
//         <Route path='/404' element={<ErrorPage/>} />
//       </Routes>
//     </BrowserRouter>
//   </React.StrictMode>
// );