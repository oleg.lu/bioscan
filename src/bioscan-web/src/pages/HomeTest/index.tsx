import { useEffect, useState } from "react";
import { Navigate } from "react-router-dom";
import { HomeTestForm } from "../../components/home-test-form";

function HomeTestPage() {

  const [authenticated, setAuthenticated] = useState<boolean>();

  useEffect(() => {
    const token = localStorage.getItem("ocean2");

    if (token != null) {
      setAuthenticated(true);
    }

  }, []);

  if (authenticated === undefined) {
    return <div>ПУСТАЯ</div>
  } else if (authenticated) {
    return <HomeTestForm/>
  }
  else return <Navigate replace to="/login" />;
};


export default HomeTestPage

