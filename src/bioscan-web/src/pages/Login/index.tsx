import { ReactNode, useEffect, useState, } from "react"
import "../../style/page-style.scss"
import { LoginForm } from "../../components/login-form";
import { Navigate } from "react-router-dom";

export function LoginPage(): ReactNode {

  const [authenticated, setAuthenticated] = useState<boolean>();

  useEffect(() => {
    const token = localStorage.getItem("ocean2");

    if (token != null) {
      setAuthenticated(true);
    }

  }, []);


  // return (
  //   <div className="login-page">
  //     <LoginForm />
  //   </div>
  // );

  if (authenticated) {
    return <Navigate replace to="/" />
  }
  else return <div className="login-page">
    <LoginForm />
  </div>;

};

