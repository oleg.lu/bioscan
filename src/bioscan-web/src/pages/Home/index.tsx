import { useEffect, useState } from "react";
import { Navigate } from "react-router-dom";
import { MainLayout } from "../../components/main-layout";
import { Carousel, Col, Row } from "antd";

function HomePage() {

    const [authenticated, setAuthenticated] = useState<boolean>();

    useEffect(() => {
        const token = localStorage.getItem("ocean2");

        if (token != null) {
            setAuthenticated(true);
        }
        else {
            setAuthenticated(false);
        }

    }, []);

    const text1 = "Обследование регулярно — это не просто медицинская рекомендация,но и залог вашего здоровья и благополучия. В современном мире, где темп жизни постоянно ускоряется, а стресс и неправильное питание становятся нормой, регулярные медицинские обследования приобретают особую важность."

    const contentCarouselStyle: React.CSSProperties = {
        height: '160px',
        color: '#fff',
        lineHeight: '160px',
        textAlign: 'center',
        background: '#364d79',
    };

    if (authenticated === undefined) {
        return <div>ПУСТАЯ</div>
    } else if (!authenticated) {
        return <Navigate replace to="/login" /> 
    } 
    else if (authenticated) { 
        return (
            <MainLayout>
            <Row style={{ padding: '30px', }}>
                <Col span={13}>
                    <h1>Обследуйтесь регулярно</h1>
                    {text1}
                </Col>
                <Col span={1}></Col>
                <Col span={9}>
                    <Carousel arrows dotPosition="left" infinite={false} effect="fade" style={{ width: '100%' }}>
                        <div>
                            <h3 style={contentCarouselStyle}>1</h3>
                        </div>
                        <div>
                            <h3 style={contentCarouselStyle}>2</h3>
                        </div>
                        <div>
                            <h3 style={contentCarouselStyle}>3</h3>
                        </div>
                        <div>
                            <h3 style={contentCarouselStyle}>4</h3>
                        </div>
                    </Carousel>
                </Col>
            </Row>
        </MainLayout>
        );
    }

};


export default HomePage

