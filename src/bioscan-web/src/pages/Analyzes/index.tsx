import { useEffect, useState } from "react";
import { Navigate } from "react-router-dom";
import { MainLayout } from "../../components/main-layout";
import { AnalysisCardForm } from "../../components/analysis-card-form";
import { Empty } from "antd";
import AuthInterceptor from "../../gears/auth-interceptor";


function AnalysisPage() {

    const [authenticated, setAuthenticated] = useState<boolean>();
    const [alalyzes, setAlalyzes] = useState<Analysis[]>([]);

    async function analyzesRequest() {
        try {
            const response = await AuthInterceptor.get<Analysis[]>('/Analysis');
            if (response.status === 200) {
                setAlalyzes(response.data)
            } else {
                console.error('Ошибка:', response.statusText);
            }
        } catch {
            setAlalyzes([])
        }
    }

    useEffect(() => {
        const token = localStorage.getItem("ocean2");

        if (token != null) {
            setAuthenticated(true);
            analyzesRequest();
        }

    }, []);

    if (authenticated === undefined) {
        return <div>ПУСТАЯ</div>
    } else if (authenticated) {
        return (
            <MainLayout>
                {alalyzes.length > 0 ? (
                    alalyzes.map((analysis, index) => (
                        <div key={analysis.id || index}>
                            <AnalysisCardForm
                                name={analysis.name}
                                description={analysis.description}
                                nomenclature={analysis.nomenclature}
                                price={analysis.price}
                                productionDay={analysis.productionDay}
                                id={analysis.id}
                            />
                        </div>
                    ))
                ) : (
                    <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />  
                )}
            </MainLayout>
        );
    }
    else return <Navigate replace to="/login" />;
};


export default AnalysisPage

