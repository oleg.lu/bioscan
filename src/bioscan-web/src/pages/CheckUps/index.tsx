import { useEffect, useState } from "react";
import { Navigate } from "react-router-dom";
import { MainLayout } from "../../components/main-layout";
import { CheckUpCardForm } from "../../components/check-up-card-form";
import AuthInterceptor from "../../gears/auth-interceptor";
import { Empty } from "antd";

function CheckUpsPage() {

    const [authenticated, setAuthenticated] = useState<boolean>();
    const [checkUps, setСheckUps] = useState<CheckUp[]>([]);


    async function suProgpamsRequest() {
        try {
            const response = await AuthInterceptor.get<CheckUp[]>('/SurveyProgram');
            if (response.status === 200) {
                setСheckUps(response.data)
            } else {
                console.error('Ошибка:', response.statusText);
            }
        } catch {
            setСheckUps([])
        }
    }

    useEffect(() => {
        const token = localStorage.getItem("ocean2");

        if (token != null) {
            setAuthenticated(true);
            suProgpamsRequest();
        }

    }, []);

    if (authenticated === undefined) {
        return <div>ПУСТАЯ</div>
    } else if (authenticated) {
        return (
            <MainLayout>
                {checkUps.length > 0 ? (
                    checkUps.map((checkUp, index) => (
                        <div key={checkUp.id || index}>
                            <CheckUpCardForm
                                name={checkUp.name}
                                description={checkUp.description}
                                price={checkUp.price}
                                professionalAppointmentsCnt={checkUp.professionalAppointmentsCnt}
                                analysisCnt={checkUp.analysisCnt}
                                id={checkUp.id}
                            />
                        </div>
                    ))
                ) : (
                    <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />  
                )}
            </MainLayout>
        );
    }
    else return <Navigate replace to="/login" />;
};


export default CheckUpsPage

